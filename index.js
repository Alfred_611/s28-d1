console.log("Hello World");

// Asynchronous Statement
// The Fetch API allows to asynchronously request for a resource(data)
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

fetch('https://jsonplaceholder.typicode.com/posts').then(response => console.log(response.status));

console.log("Goodbye");

// Retrieve contents/data from teh "Response" object
fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response.json()).then((json) => console.log(json));